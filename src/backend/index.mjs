import Router from "@koa/router";
import JSDB from "@small-tech/jsdb";
import Koa from "koa";
import cors from "@koa/cors";

const app = new Koa();
const router = new Router();
const db = JSDB.open("db");

// logger
app.use(async (ctx, next) => {
  await next();
  const rt = ctx.response.get("X-Response-Time");
  console.log(`${ctx.method} ${ctx.url} - ${rt}`);
});

// x-response-time
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set("X-Response-Time", `${ms}ms`);
});

app.use(cors({ origin: false }));

/**
 * @openapi
 *  /accounts/{id}:
 *    get:
 *      summary: Gets an account by id
 *      description: Returns the account
 *      parameters:
 *       - name: id
 *         description: Id of account
 *         in: path
 *         required: true
 *         type: string
 *      responses:
 *        200:
 *          description: success
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    format: number
 *                    required: true
 *                    example: 42
 *                  balance:
 *                    type: integer
 *                    format: number
 *                    required: true
 *                    example: 4711.95
 *                  accountNumber:
 *                    type: string
 *                    required: true
 *                    example: NL42 FISH 0000 1234 56
 *                  accountHolder:
 *                    type: string
 *                    required: true
 *                    example: Garfield
 *                  transactions:
 *                    type: array
 *                    required: false
 *                    items:
 *                      type: object
 *                      required:
 *                        - title
 *                        - amount
 *                      properties:
 *                        title:
 *                          type: string
 *                        amount:
 *                          type: integer
 *                          format: number
 *        204:
 *          description: no content
 */
router.get("/accounts/:id", (ctx) => {
  const { id } = ctx.params;
  const account = db.accounts.find((it) => it.id === +id);

  ctx.body = account;
});

// response
app.use(router.routes());

app.listen(3000, () => {
  console.info("Server running on http://localhost:3000");

  if (!db.accounts) {
    /** @type {Array<BackendAccount>} */
    db.accounts = [
      {
        id: 1,
        accountHolder: "Luna",
        accountNumber: "NL42 FISH 0000 3542 34",
        balance: 465.95,
        transactions: [
          {
            title: "Fish Food",
            amount: -12.35,
          },
          {
            title: "Sushi Food",
            amount: -22.55,
          },
          {
            title: "Cuddles",
            amount: 5.44,
          },
          {
            title: "Moral Support",
            amount: 15.36,
          },
        ],
      },
    ];
  }
});
