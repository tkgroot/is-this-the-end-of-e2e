import { css, html, LitElement } from "lit";
import "./components/cat-account-details";
import "./components/cat-button";
import "./components/cat-header";
import "./components/cat-transactions";
import { CurrentAccountService } from "./services/CurrentAccountService.js";
import { cardStyle } from "./styles/cardStyle";
import { utilities } from "./styles/utilities.js";

/**
 * Cat account element displaying the current account details of a cat.
 *
 * @prop _account the account in view
 */
export class CatAccount extends LitElement {
  static get properties() {
    return {
      _account: { type: Object },
    };
  }

  static styles = [
    utilities,
    cardStyle,
    css`
      :host {
        display: flex;
        flex-flow: column nowrap;
      }
    `,
  ];

  constructor() {
    super();
    this._account;
  }

  async connectedCallback() {
    super.connectedCallback();
    this.service = new CurrentAccountService();
    this._account = await this.service.getAccount();
  }

  render() {
    if (!this._account) return html`Loading...`;

    const { accountNumber, accountHolder, balance, transactions } =
      this._account;

    return html`
      <cat-header></cat-header>
      <div class="mt-10 w-[48rem] m-auto">
        <div class="flex items-center justify-between mb-10">
          <h2 class="text-gray-600 font-light">Current Account</h2>
          <cat-button>Manage</cat-button>
        </div>

        <div class="mb-10">
          <cat-account-details
            holder="${accountHolder}"
            accountNumber="${accountNumber}"
            balance="${balance}"
          ></cat-account-details>
        </div>

        <div class="card text-black mb-4">
          <div class="card__content">
            <h2 class="text-orange-500 my-2">Transactions</h2>
            <hr class="w-full border-gray-200 border-solid" />
            <cat-transactions
              .transactions="${transactions}"
            ></cat-transactions>
          </div>
        </div>
      </div>
    `;
  }
}

window.customElements.define("cat-account", CatAccount);
