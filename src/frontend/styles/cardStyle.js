import { css } from "lit";

export const cardStyle = css`
  .card {
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 8px;
    box-shadow: 12.5px 12.5px 10px rgba(0, 0, 0, 0.035),
      100px 100px 80px rgba(0, 0, 0, 0.07);
    display: flex;
    flex: auto;
    padding: 1rem;
  }

  .card__content {
    display: flex;
    flex-flow: column nowrap;
    width: 100%;
  }
`;
