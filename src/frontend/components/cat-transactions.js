import "@lion/accordion/define";
import { classMap } from "@lion/core";
import { html, LitElement } from "lit";
import { utilities } from "../styles/utilities";
import "./cat-transaction-list-content";
import "./cat-transaction-list-item";

/**
 * @attr iban iban number of the transaction
 */
export class CatTransactions extends LitElement {
  static properties = {
    transactions: { type: Object },
  };

  static styles = [utilities];

  constructor() {
    super();
    /** @type {Accounts.AccountsDetail.ResponseBody['transactions']} */
    this.transactions;
  }

  render() {
    return html`
      ${this.transactions?.map(
        ({ title, amount }) => html`
          <lion-accordion>
            <div slot="invoker">
              <cat-transaction-list-item>
                <span slot="title">${title}</span>
                <span class="${classMap({ "text-red-600": amount < 0 })}"
                  >${amount}</span
                >
              </cat-transaction-list-item>
            </div>
            <div slot="content">
              <cat-transaction-list-content
                .date="${new Date().toDateString()}"
                type="Card payment"
              ></cat-transaction-list-content>
            </div>
          </lion-accordion>
        `
      )}
    `;
  }
}

customElements.define("cat-transactions", CatTransactions);
