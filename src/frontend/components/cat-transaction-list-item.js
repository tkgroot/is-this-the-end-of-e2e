import { html, LitElement } from "lit";
import { utilities } from "../styles/utilities";

/**
 * @attr iban iban number of the transaction
 */
export class CatTransactionListItem extends LitElement {
  static properties = {
    title: { type: String },
    iban: { type: String },
    balance: { type: Number },
  };

  static styles = [utilities];

  constructor() {
    super();
    /** @type {string} */
    this.title;
    /** @type {string} */
    this.iban;
    /** @type {number} */
    this.balance;
  }

  render() {
    return html`
      <div
        class="flex flex-row justify-between border-solid border border-transparent border-b-gray-200  pb-2 mb-2 cursor-pointer"
      >
        <slot name="title" class="font-bold">${this.title}</slot>
        <slot class="font-bold">${this.balance}</slot>
      </div>
    `;
  }
}

customElements.define("cat-transaction-list-item", CatTransactionListItem);
