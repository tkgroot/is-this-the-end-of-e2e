import { LionButton } from "@lion/button";
import { css } from "lit";

export class CatButton extends LionButton {
  static styles = [
    ...super.styles,
    css`
      :host {
        background-color: #ee7809;
        border: 1px solid transparent;
        border-radius: 8px;
        color: #fff;
        cursor: pointer;
        font-family: inherit;
        font-size: 1.1em;
        font-weight: 700;
        padding: 0.6em 1.2em;
        transition: border-color 0.25s;
      }

      :host(:hover) {
        background-color: #ee7809;
        box-shadow: 6.5px 6.5px 13.3px rgba(0, 0, 0, 0.15),
          26.8px 26.8px 44.7px rgba(0, 0, 0, 0.098),
          125px 125px 200px rgba(0, 0, 0, 0.075);
      }

      :host(:focus),
      :host(:focus-visible) {
        outline: 4px auto -webkit-focus-ring-color;
      }

      :host([outline]) {
        background-color: transparent;
        color: orange;
      }

      :host([outline]:hover) {
        border-color: #ee7809;
        box-shadow: unset;
      }

      @media (prefers-color-scheme: light) {
        :host {
          background-color: #f9f9f9;
        }
      }
    `,
  ];
}

customElements.define("cat-button", CatButton);
