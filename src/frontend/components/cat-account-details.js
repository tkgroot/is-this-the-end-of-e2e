import { html, LitElement } from "lit";

export class CatAccountDetails extends LitElement {
  static properties = {
    holder: { type: String },
    accountNumber: { type: String },
    balance: { type: Number },
  };

  constructor() {
    super();
    /** @type {string} */
    this.holder;
    /** @type {string} */
    this.accountNumber;
    /** @type {number} */
    this.balance;
  }

  render() {
    return html`
      <div class="flex flex-row justify-between mb-2">
        <div class="flex flex-col">
          <span
            class="font-extrabold text-2xl text-gray-600 mb-2"
            name="accountHolder"
            >${this.holder}</span
          >
          <span class="font-thin text-gray-500">${this.accountNumber}</span>
        </div>
        <div class="flex gap-2 text-3xl text-gray-500">
          <span>EUR</span>
          <span class="font-bold" name="accountBalance"
            >${+this.balance.toFixed(2)}</span
          >
        </div>
      </div>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("cat-account-details", CatAccountDetails);
