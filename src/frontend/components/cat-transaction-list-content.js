import { html, LitElement } from "lit";
import { utilities } from "../styles/utilities";

/**
 * @attr iban iban number of the transaction
 */
export class CatTransactionListContent extends LitElement {
  static properties = {
    date: { type: String },
    description: { type: String },
    type: { type: String },
  };

  static styles = [utilities];

  constructor() {
    super();
    /** @type {Date} */
    this.date;
    /** @type {string} */
    this.description;
    /** @type {string} */
    this.type;
  }

  render() {
    return html`
      <div class="ml-10">
        <div class="flex mb-2">
          <span class="text-gray-500 flex-1">Description</span>
          <span class="flex-grow">${this.description}</span>
        </div>
        <div class="flex mb-2">
          <span class="text-gray-500 flex-1">Date</span>
          <span class="flex-grow">${this.date}</span>
        </div>
        <div class="flex mb-2">
          <span class="text-gray-500 flex-1">Type</span>
          <span class="flex-grow">${this.type}</span>
        </div>
      </div>
    `;
  }
}

customElements.define(
  "cat-transaction-list-content",
  CatTransactionListContent
);
