import { html, LitElement } from "lit";

export class CatHeader extends LitElement {
  render() {
    return html`
      <div class="flex bg-white text-orange-500">
        <img src="../assets/logo.png" alt="purrING logo" height="100" />
      </div>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("cat-header", CatHeader);
