import { test, expect } from "@playwright/test";

test("should load account and displays correctly", async ({ page }) => {
  await page.goto("http://localhost:5173");

  // create a locator
  const getStarted = page.getByText("Get Started");
  const accountHolder = await page.locator('[name="accountHolder"]');
  const accountBalance = await page.locator('[name="accountBalance"]');

  // Expect an attribute "to be strictly equal" to the value.
  await expect(accountHolder).toContainText("Luna");
  await expect(accountBalance).toContainText("465.95");
});
