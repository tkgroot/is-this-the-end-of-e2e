/** @implements {AccountService} */
export class ServiceAccount {
  getAccount = async () => {
    /** @type {Accounts.AccountsDetail.ResponseBody} */
    const { accountHolder, accountNumber, balance, transactions } = await fetch(
      "http://localhost:3000/accounts/1"
    ).then((res) => res.json());

    return {
      accountHolder,
      accountNumber,
      balance,
      transactions,
    };
  };
}
