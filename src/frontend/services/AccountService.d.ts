type ExternalAccount = Accounts.AccountsDetail.ResponseBody;
export type Account = Omit<ExternalAccount, "id">;

export interface CurrentAccountService {
  getAccount(): Promise<Account>;
}
