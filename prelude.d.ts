import { BackendAccount as _BackendAccount } from "./src/backend/Accounts";
import { Accounts as _Accounts } from "./src/frontend/schemas/AccountsRoute";
import {
  Account as _Account,
  AccountService as _AccountService,
  Transaction as _Transaction,
} from "./src/frontend/services/AccountService";

declare global {
  export {
    _BackendAccount as BackendAccount,
    _Accounts as Accounts,
    _Account as Account,
    _AccountService as AccountService,
    _Transaction as Transaction,
  };
}
