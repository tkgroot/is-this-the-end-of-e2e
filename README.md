# Is this the end of E2E?

## Install

```bash
npm i
```

This will setup and configure the repository, meaning that it will generate the backend contract (swagger.json) place it in the `specs` directory and generates types and interfaces which are located in `src/frontend/schemas`.

Two different schemas are generated. For the demo the `AccountRoute.ts` has been used.

## Change

To see what impact a change has on the type generation, you have to follow the steps below:

1. open the `src/backend/index.mjs` and find line 52. Change the `balance` into `accountBalance`.

2. change the database account entries to match accountBalance, see below.

```diff
db.accounts = [
      {
        id: 1,
        accountHolder: "Luna",
        accountNumber: "NL42 FISH 0000 3542 34",
-       balance: 465.95,
+       accountBalance: 465.95,
```

3. instantiate a fresh database by executing `npm run clear:db`

4. start the backend service `npm run start:server` (This is necessary to re-instantiate the database.)

5. regenerate the API contract and generate the types `npm run prepare`

## Notes

There are two packages used to generate the types from the contract.

- [openapi-typescript](https://github.com/drwpow/openapi-typescript)
- [swagger-typescript-api](https://github.com/acacode/swagger-typescript-api)

The `swagger-typescript-api` uses a script to generate the types. It is located in `scripts/generate-types.cjs`.

It requires that the API is specified in the `package.json` under the custom key `apis`

```json
{
  "apis": [
    {
      "name": "CatAccountApi",
      "version": "1.0.0"
    }
  ]
}
```
