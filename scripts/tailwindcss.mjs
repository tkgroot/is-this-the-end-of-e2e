import { watch } from 'chokidar';
import { writeFile } from 'fs/promises';
import postcss from 'postcss';
import postcssLit from 'postcss-lit';
import * as prettier from 'prettier';
import tailwindcss from 'tailwindcss';

const filename = 'utilities.js';
const filepath = `./src/frontend/styles/${filename}`;

const ingUtilityStyles = `
 import {css} from 'lit';

 export const utilities = css\`
  @tailwind utilities;
\``;

const processIngCss = (/** @type {string} */ file) =>
  postcss([tailwindcss])
    .process(file, { from: undefined, syntax: postcssLit })
    .then(({ css }) => css);

const formatFile = (/** @type {string} */ file = '') =>
  prettier.format(file, { filepath: '.js' });

function run() {
  Promise.resolve(ingUtilityStyles)
    .then(processIngCss)
    .then(formatFile)
    .then(file => writeFile(filepath, file, 'utf-8'))
    .then(() => {
      // eslint-disable-next-line no-console
      console.info('utilities styles created.');
    });
}

// eslint-disable-next-line no-unused-expressions
process.argv.includes('--watch')
  ? watch('src/frontend/**/*', {
      ignored: path => path.includes(filename),
    }).on('change', run)
  : run();
