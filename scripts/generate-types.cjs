#!/usr/bin/env node

const { generateApi } = require("swagger-typescript-api");
const { join, resolve } = require("path");
const { readFile } = require("fs").promises;

const { cwd } = process;
const { error, log } = console;

// const INPUT_DIR = resolve(cwd(), ".swagger");
const INPUT_DIR = resolve(cwd(), "specs");
// const OUTPUT_DIR = resolve(cwd(), "packages/services/types/external");
const OUTPUT_DIR = resolve(cwd(), "src/frontend/schemas");
const packageJsonFile = resolve(cwd(), "package.json");

/**
 * @typedef {{name: string, version: string}} Api
 */

/** @param {Buffer} buffer */
const bufferToString = (buffer) => buffer.toString("utf-8");

/** @param {string} str */
const toJson = (str) => JSON.parse(str);

const packageJson = readFile(packageJsonFile).then(bufferToString).then(toJson);

/** @param {{apis: Api[] }} _ */
const getApis = ({ apis }) => apis ?? [];

/**
 * @param {string} str
 * @param {boolean} lowerCase
 */
const createSlugFrom = (str, lowerCase = true) => {
  const slug = str.replace(/ +/g, "-").replace(/[’,.:()!?]+/g, "");
  return lowerCase ? slug.toLowerCase() : slug;
};

/** @param {Api} _ */
const generateDefinitions = ({ name, version }) =>
  generateApi({
    name,
    input: join(INPUT_DIR, `${createSlugFrom(name, false)}-${version}.json`),
    output: OUTPUT_DIR,
    cleanOutput: true,
    moduleNameFirstTag: true,
    modular: true,
    toJS: false,
    generateRouteTypes: true,
    generateClient: false,
  });

/** @param {Api[]} arr */
const generate = (arr = []) => Promise.all(arr.map(generateDefinitions));

packageJson.then(getApis).then(generate).catch(error);
