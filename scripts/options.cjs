const fs = require("fs");
const { swagger } = JSON.parse(fs.readFileSync("./package.json"));

module.exports = swagger;
