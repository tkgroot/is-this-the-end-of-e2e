/* eslint-disable */
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: {
    files: [
      "./src/frontend/**/*.{html,js}",
      "!./src/frontend/styles/**/*.{html,js}",
    ],
  },
  corePlugins: {
    filter: false,
    transform: false,
  },
};
